#ifndef MERGESORT_HPP_INCLUDED
#define MERGESORT_HPP_INCLUDED

#include <vector>
using namespace std;

namespace Algo
{
    namespace MergeSort
    {
        void merge(vector<int>& left, vector<int>& right, vector<int>& input);
        void sort(vector<int>& input);
        void print(vector<int>& input);
    }
}
#endif // MERGESORT_HPP_INCLUDED
