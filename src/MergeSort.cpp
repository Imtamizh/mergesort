#include <iostream>
#include "MergeSort.hpp"

void Algo::MergeSort::merge(vector<int>& left, vector<int>& right, vector<int>& input)
{
    int leftSize = left.size();
    int rightSize = right.size();
    int i = 0;
    int j = 0;
    int k = 0;

    //Compare left & right sub array and merge it into output array
    while(i < leftSize && j < rightSize)
    {
        if(left[i] < right[j])
        {
            input[k] = left[i];
            i++;
            k++;
        }
        else
        {
            input[k] = right[j];
            j++;
            k++;
        }
    }

    //copy if left sub array has any remaining elements
    while(i < leftSize)
    {
        input[k] = left[i];
        i++;
        k++;
    }

    //copy if right sub array has any remaining elements
    while(j < rightSize)
    {
        input[k] = right[j];
        j++;
        k++;
    }
}

void Algo::MergeSort::sort(vector<int>& input)
{
    vector<int> left;
    vector<int> right;
    int mid;
    int n;

    //cleanup
    left.clear();
    right.clear();

    n = input.size();

    //base case
    if(n < 2)
    {
        return;
    }

    //find mid element
    mid = n/2;

    for(int i = 0; i < mid; i++)
    {
        left.push_back(input[i]);
    }

    for(int i = mid; i < n; i++)
    {
        right.push_back(input[i]);
    }

    //sort left sub array
    sort(left);

    //sort right sub array
    sort(right);

    //merge
    merge(left, right, input);
}


void Algo::MergeSort::print(vector<int>& input)
{
    for(int i = 0; i < input.size(); i++)
    {
        cout << input[i] << "\t";
    }

    cout << endl;
}
