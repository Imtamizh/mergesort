#include <iostream>
#include "MergeSort.hpp"

using namespace Algo::MergeSort;

int main()
{
    cout << "### Merge Sort ###" << endl;

    vector<int> input({100, 90, 40, 20, 10, 5, 3, 1});

    cout << "Unsorted List:";

    print(input);

    cout << "Sorted List:";

    sort(input);

    print(input);

    return 0;
}
